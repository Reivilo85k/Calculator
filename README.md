# Postfix prefix calculator README.md 
Operators: +-*/
Postfix Examples: 1 2 +
Postfix Examples: 5 15 /
Prefix Examples: - 2 4
Prefix Examples: * 11# Postfix prefix calculator
Use colors in the commandline prints(B) (Google it)
Menu: 0-Choose between postfix prefix calculations
Menu: 1-Save the result in a file(B) 2-Print the result on screen
Menu: 3-Read from the file(B)
Hints: 1- You should not use hardcoded nested if-else statements for choosing between operators.
Hints: 2- You have to handle exceptions
Hints: 3- Try to implement it with usage of generic types

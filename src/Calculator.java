import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Calculator implements Methods<Calculator> {
    private Character operator;
    ArrayList integersList;
    BigDecimal result = BigDecimal.ZERO;

    //To create an empty Calculator
    public Calculator() {
    }

    //To initialize a Calculator
    public Calculator(char operator, ArrayList integerList) {
        this.operator= operator;
        this.integersList = integerList;
    }

    public char getOperator() {
        return operator;
    }

    public void setOperator(char iterator) {
        this.operator = iterator;
    }

    public ArrayList getIntegerList() {
        return integersList;
    }

    public void setValue1(ArrayList integersList) {
        this.integersList = integersList;
    }

    @Override
    public void add(Calculator calculator) {
        //Created temp because it is not possible to directly cast an ArrayList of integers to BigDecimal
        //But it is possible to cast an Integer to it
        //temp is meant to create an integer with the value of a value contained in the ArrayList of integers (aka the values we want to operate)
        Integer temp = 0;
        for (int i = 0; i < integersList.size();i++) {
            //temp is now equal to the first value of the array of integers
            temp = (Integer) getIntegerList().get(i);
            //we add the other values to it and save to result
            result = result.add(BigDecimal.valueOf(temp));
        }
    }

    @Override
    public  void subtract(Calculator calculator) {
        //Created temp because it is not possible to directly cast an ArrayList of integers to BigDecimal
        //But it is possible to cast an Integer to it
        //temp is meant to create an integer with the value of a value contained in the ArrayList of integers (aka the values we want to operate)
        Integer temp = 0;
        //result now equals the first value of the array
        result = BigDecimal.valueOf((Integer) getIntegerList().get(0));
        for (int i = 1; i < integersList.size();i++) {
            //temp equals the second value of the array
            temp = (Integer) getIntegerList().get(i);
            //operating subsequent values of the array and saving to result
            result = result.subtract(BigDecimal.valueOf(temp));
        }
    }

    @Override
    public void multiply(Calculator calculator) {
        //Created temp because it is not possible to directly cast an ArrayList of integers to BigDecimal
        //But it is possible to cast an Integer to it
        //temp is meant to create an integer with the value of a value contained in the ArrayList of integers (aka the values we want to operate)
        Integer temp = 0;
        //result now equals the first value of the array
        result = BigDecimal.valueOf((Integer) getIntegerList().get(0));
        for (int i = 1; i < integersList.size();i++) {
            //temp equals the second value of the array
            temp = (Integer) getIntegerList().get(i);
            //operating subsequent values of the array and saving to result
            result = result.multiply(BigDecimal.valueOf(temp));
        }
    }

    @Override
    public void divide(Calculator calculator) {
        //Created temp because it is not possible to directly cast an ArrayList of integers to BigDecimal
        //But it is possible to cast an Integer to it
        //temp is meant to create an integer with the value of a value contained in the ArrayList of integers (aka the values we want to operate)
        Integer temp = 0;
        //result now equals the first value of the array
        result = BigDecimal.valueOf((Integer) getIntegerList().get(0));
        for (int i = 1; i < integersList.size();i++) {
            //temp equals the second value of the array
            temp = (Integer) getIntegerList().get(i);
            //operating subsequent values of the array and saving to result
            result = result.divide((BigDecimal.valueOf(temp)), 2);
        }
    }

    public void choseProcess(Calculator operation){
        //Creating a Hashmap listing all the operations the calculator cam handle
        //the key is the Char input by the user (+, -, * or /) and a non-static method is associated
        Map<Character, Runnable> operators = new HashMap<>();
        operators.put((char) 43, () -> operation.add(operation));
        operators.put((char) 45, () -> operation.subtract(operation));
        operators.put((char) 42, () -> operation.multiply(operation));
        operators.put((char) 47, () -> operation.divide(operation));

        // cmd equals the operator chosen by the user
        char cmd = operation.getOperator();
        // calling the void method associated to the operator in the above hashmap
        operators.get(cmd).run();
        }

    @Override
    public void saveResult(BigDecimal result) throws IOException {
        //Saving the BigDecimal result as a String
        List<String> fileLines = Arrays.asList("The result is : " + result.toString());
        //path where the file will be created
        Path relativePath = Paths.get("Calculator/src/Result.txt");
        //deleting any file that could cause a conflict
        Files.deleteIfExists(relativePath);
        //creating the file
        Files.createFile(relativePath);
        //Writing the result in the file
        Files.write(relativePath, fileLines);
        //letting the user know file is created
        System.out.println((char)27 + "[35m" + "Calculator/src/Result.txt was created");
        }
}

import java.io.IOException;
import java.math.BigDecimal;

public interface Methods <T> {
    void add(T t);

    void subtract(T t);
    void multiply(T t);
    void divide(T t);
    void saveResult(BigDecimal result) throws IOException;
}

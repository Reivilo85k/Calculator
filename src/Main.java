import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    //# Postfix prefix calculator
    //Operators: +-*/
    //Postfix Examples: 1 2 +
    //Postfix Examples: 5 15 /
    //Prefix Examples: - 2 4
    //Prefix Examples: * 11# Postfix prefix calculator
    //Use colors in the commandline prints(B) (Google it)
    //Menu: 0-Choose between postfix prefix calculations
    //Menu: 1-Save the result in a file(B) 2-Print the result on screen
    //Menu: 3-Read from the file(B)
    //Hints: 1- You should not use hardcoded nested if-else statements for choosing between operators.
    //Hints: 2- You have to handle exceptions
    //Hints: 3- Try to implement it with usage of generic types

    public static void main(String[] args) throws Exception {
        System.out.println((char)27 + "[31m" + "PREFIX/POSTFIX CALCULATOR STARTING\n");
        //deleting eventual preexisting file from previous run of the program
        Files.deleteIfExists(Paths.get("Calculator/src/Result.txt"));

        //Creates new Menu object. Menu object creates a Calculator object and keeps track of the result BigDecimal
        Menu menu = new Menu();

        //launch Menu method menu1
        menu.menu1();
        //launch Menu method menu1
        menu.menu2();
    }
}

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Menu {

    Scanner scanner = new Scanner(System.in);
    private Calculator operation = new Calculator();
    BigDecimal result = BigDecimal.ZERO;

    public void menu1() throws Exception {

        //Input the Calculator variables. the loop handles exceptions.
        for (int i = 0; i < 1; i++) {

            //menu1Text launch a method displaying the menu text.
            menu1Text();
            Integer userInput = scanner.nextInt();

            //Make sure input is either 1 or 2
            if (userInput != 1 && userInput != 2) {
                //Restart loop in case on wrong input
                i--;
                System.out.println("\nINCORRECT ARGUMENT\n");
            } else {
                //launch the method where we input the Calculator values
                operation = inputValues(scanner);
                //Launch the method deciding which operation to process depending of the input operator
                operation.choseProcess((operation));
                //Just fancy folklore
                System.out.println("PROCESSING...\n");
            }
        }
    }

    public void menu1Text(){
        //Plain boring text
        //The unusual syntax is meant for colored command lines in a Windows console
        System.out.println((char) 27 + "[34m" + "Please select the method of calculation");
        System.out.println((char) 27 + "[34m" + "1. Prefix calculation : Enter an iterator (-, +, * or /) followed by two numbers to be processed");
        System.out.println((char) 27 + "[34m" + "2. Postfix calculation : Enter two numbers to be processed followed by an iterator (-, +, * or /)");
        System.out.println((char) 27 + "[31m" + "Select option 1 or 2");
    }

    public void menu2() throws IOException {
        //first loop is meant to let the user decide when to close program
        for (int j = 0; j < 1; ) {
            //second loop handles exceptions in case of wrong input
            for (int i = 0; i < 1; i++) {
                //menu2Text launch a method displaying the menu text.
                Menu2Text();
                Integer userInput1 = scanner.nextInt();

                if (userInput1 != 1 && userInput1 != 2 && userInput1 != 3 && userInput1 != 4) {
                    //checking if the input is valid
                    i--;
                    System.out.println("\nINCORRECT ARGUMENT\n");
                } else if (userInput1 == 1) {
                    //launching the method the creates a file
                    operation.saveResult(operation.result);
                } else if (userInput1 == 2) {
                    //printing result without creating a file
                    System.out.println(operation.result);
                } else if (userInput1 == 3) {
                    //read previously created file
                    if (Files.exists(Paths.get("Calculator/src/Result.txt"))) {
                        System.out.println((char) 27 + "[35m" + Files.readAllLines(Paths.get("Calculator/src/Result.txt")));
                    }// handles exception if the user wants to read a file without creating it first
                    else {
                        System.out.println("ERROR : File not found, Please create file before reading from it");
                    }
                } else if (userInput1 == 4) {
                    //The program deletes the result file before completion
                    System.out.println("CLOSING CALCULATOR");
                    Files.deleteIfExists(Paths.get("Calculator/src/Result.txt"));
                    j++;
                }
            }
        }
    }

    public void Menu2Text(){
        //plain boring text
        //The unusual syntax is meant for colored command lines in a Windows console
        System.out.println("\n");
        System.out.println((char) 27 + "[34m" + "Please select an option");
        System.out.println((char) 27 + "[34m" + "1. Save the result in a file");
        System.out.println((char) 27 + "[34m" + "2. Print the result on screen");
        System.out.println((char) 27 + "[34m" + "3. Read from result file");
        System.out.println((char) 27 + "[34m" + "4. Close calculator");
        System.out.println((char) 27 + "[31m" + "Select option 1, 2,3 or 4");
    }

    public Calculator inputValues(Scanner scanner) {
        char operator = choseOperator(scanner);
        Integer number = 0;
        String input;
        ArrayList<Integer> integersList = new ArrayList<Integer>();

        for (int i = 0; i < 1; i++) {
            //input first integer
            System.out.println("Please chose first integer");
            number = scanner.nextInt();
            integersList.add(number);
            //Starting a loop to let user input as many integers he choses to input
            for (int j = 0; j < 1; ) {
                //second integer input is mandatory
                System.out.println("Please chose next integer");
                number = scanner.nextInt();
                integersList.add(number);
                j++;
                //letting user chose if he wants to input more integers to operate
                System.out.println("PRESS F if you want to add another integer or any other button to start the calculation");
                input = scanner.next();
                if (input.equals("F")){
                    j--;
                }
            }
        }
        return new Calculator(operator, integersList);
    }
    public char choseOperator (Scanner scanner){
        //loop for operator input.
        // It is separated from integer input to let user go back to operator input right away in case of mistake
        char operator = 0;
        for (int i = 0; i < 1; i++) {
            System.out.println("Please chose Iterator (+, -, * or /)");
            operator = scanner.next().charAt(0);
            //checking if the operator input is correct
            if (operator != 42 && operator != 43 && operator != 45 && operator != 47) {
                System.out.println("\nUNRECOGNIZED ITERATOR, PLEASE RETRY\n");
                i--;
            }
        }return operator;
    }
}
